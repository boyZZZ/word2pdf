package com.wisedu.pdf.web;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wisedu.pdf.service.Word2PdfService;

/**
 * 
 */
@WebServlet("/word2pdf")
@MultipartConfig
@SuppressWarnings("serial")
public class Word2PdfServlet extends HttpServlet {

	private String uploadDir;

	@Override
	public void init(ServletConfig config) throws ServletException {

		super.init(config);

		uploadDir = config.getInitParameter("uploadDir");
		
		if (uploadDir == null) {
			
			uploadDir = config.getServletContext().getRealPath("/upload");
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		Word2PdfService word2Pdf = new Word2PdfService();
		
		String pdfPath = word2Pdf.convert(request.getPart("file"), uploadDir);
		
		if (pdfPath != null) {

			response.setContentType("application/pdf");
			
			word2Pdf.write(pdfPath, response.getOutputStream());
		}
		
	}
}
